package com.myuplay.CSVCombiner;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javafx.concurrent.Task;

/**
 * Handles a single file.
 * @author Tyler
 *
 */
@SuppressWarnings("restriction")
public class FileParser extends Task<Boolean>{

	private List<File> files = new ArrayList<File>();

	private final BufferedWriter out;
	private final String delimiter;
	private final Pattern filePattern;

	public FileParser(File top, File output, String delimiter, Pattern filePattern) throws IOException{

		this.delimiter = delimiter;
		this.filePattern = filePattern;

		if (output.canWrite() || !output.exists()){
			FileWriter fw = new FileWriter(output);
			out = new BufferedWriter(fw);
		} else {
			Console.error("Could not write to the file!");
			throw new IOException("Could not write to file.");
		}

		if (top.isDirectory()){
			Console.log("Searching " + top.getName() + " for csv files.");
			getFiles(top);
			Console.log("Found " + files.size() + " files.");
		} else {
			if (top.exists() && top.getName().endsWith(".csv")){
				files.add(top);
			} else {
				Console.warn("Invalid file: " + top.getPath());
				Console.log("All files must be csv files! No parsing will be done.");
			}
		}

	}

	private void getFiles(File top){

		for (File f : top.listFiles()){

			if (f.isDirectory()){
				getFiles(f);
			} else {
				if (f.exists() && filePattern.matcher(f.getName()).matches()){
					files.add(f);
				}
			}

		}

	}

	public void parse(){

		final String eol = System.lineSeparator();

		for (int i = 0; i < files.size(); ++i){

			File f = files.get(i);

			if (f.canRead()){
				try {

					BufferedReader reader = Files.newBufferedReader(f.toPath(), Charset.forName("US-ASCII"));
					String line = null;

					String name = f.getName();

					//Parse lines
					while ((line = reader.readLine()) != null){
						if (line.startsWith("#")) continue;

						out.write((name + delimiter + line + eol).replace(delimiter, ","));

					}

				} catch (IOException e) {
					Console.warn("An error occured in file: " + f.getPath() + "! File skipped.");
					Console.error(e.getMessage());
					e.printStackTrace();
				}
			} else {
				Console.warn("Can't read: " + f.getPath() + "! Skipping.");
			}

			updateProgress(i, files.size());
			updateMessage("Parsing file " + i + "/" + files.size());

		}

	}

	public Boolean call(){

		try {
			parse();
			done();
			return new Boolean(true);
		} catch (Exception e){
			Console.error("An error occurred:", e.getMessage());
			updateMessage("Failed");
			failed();
			return new Boolean(false);
		}

	}

}
