package com.myuplay.CSVCombiner;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.control.*;
import javafx.beans.value.*;

@SuppressWarnings("restriction")
public class Main extends Application {

	private static Stage consoleWindow;
	private static final TextArea console = new TextArea();
	private static File directory = new File(System.getProperty("user.dir"));
	private static File output = new File(directory.getPath() + "/combined.csv");
	private static Stage window;
	private static final ProgressBar prog = new ProgressBar();
	private static final Text status = new Text("Waiting...");

	@Override
	public void start(Stage stage) throws Exception {

		window = stage;

		BorderPane bp = new BorderPane();

		bp.setTop(createTitle());
		bp.setCenter(createInputs());

		Scene scene = new Scene(bp);

		stage.setScene(scene);
		stage.setTitle("CSV-Combiner");
		stage.show();

		consoleWindow = new Stage();

		openConsole();

		consoleWindow.show();

		stage.setOnCloseRequest(new EventHandler<WindowEvent>(){
			public void handle(WindowEvent arg0) {
				consoleWindow.close();
			}
		});

		stage.toFront();

	}

	private Pane createTitle(){

		HBox title = new HBox();
		title.setPadding(new Insets(15, 12, 15, 15));
		title.setStyle("-fx-background-color: #336699");

		Text t = new Text("CSV Combiner");

		t.setFont(new Font(24));

		title.getChildren().add(t);

		return title;

	}

	private Pane createInputs(){

		final AtomicBoolean regex = new AtomicBoolean(true), del = new AtomicBoolean(false);
		final Button run = new Button("Run");

		//This is the master list of items on the screen.
		VBox list = new VBox();

		//Create the variable inputs.

		BorderPane vars = new BorderPane();

		final TextField regexFileMatcher = new TextField();
		regexFileMatcher.setText(".*\\.csv");
		regexFileMatcher.setTooltip(
				new Tooltip("This must be a valid regex string. For more info: http://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html")
				);

		regexFileMatcher.textProperty().addListener(new ChangeListener<String>(){
			@Override
			public void changed(final ObservableValue<? extends String> observable, final String oldValue, final String newValue) {
				// this will run whenever text is changed
				//TODO Check for validity.
				try{
					Pattern.compile(newValue);
				} catch (Exception e){
					regexFileMatcher.setStyle("-fx-border-color: red; -fx-border-radius: 5px");
					Console.log("Invalid regex.");
					regex.set(false);
					run.setDisable(true);
					return;
				}

				regex.set(true);
				regexFileMatcher.setStyle("");

				if (del.get()){
					run.setDisable(false);
				}


			}
		});

		final ComboBox<String> delimiter = new ComboBox<String>();
		delimiter.setEditable(true);
		delimiter.setStyle("-fx-border-color: red; -fx-border-radius: 5px");
		delimiter.valueProperty().addListener(new ChangeListener<String>(){

			@Override
			public void changed(ObservableValue<? extends String> arg0,
					String old, String newVal) {
				if (newVal.isEmpty()){
					Console.log("Delimiter cannot be empty.");
					del.set(false);
					run.setDisable(true);
					delimiter.setStyle("-fx-border-color: red; -fx-border-radius: 5px");
				} else {
					del.set(true);
					delimiter.setStyle("");
					if (regex.get()){
						run.setDisable(false);
					}
				}

			}

		});

		delimiter.setTooltip(
				new Tooltip("This is the value that separates the values in your files. Use <tab> for tab, it will be replaced with a real tab.")
				);
		delimiter.getItems().addAll(
				"<tab>",
				"<space>",
				","
				);


		vars.setLeft(regexFileMatcher);
		vars.setRight(delimiter);

		//Create buttons and file intputs.

		BorderPane in = new BorderPane();

		//Input directory.
		final TextField idir = new TextField();
		idir.setPrefWidth(300);
		idir.setText(directory.getPath());
		idir.setEditable(false);
		Button infile = new Button("Input Dir");
		infile.setPrefWidth(100);
		infile.setOnAction(new EventHandler<ActionEvent>(){
			public void handle(ActionEvent arg0) {
				DirectoryChooser dc = new DirectoryChooser();
				dc.setInitialDirectory(directory);
				dc.setTitle("Input directory");
				File tmp = dc.showDialog(window);
				if (tmp != null){
					directory = tmp;
					idir.setText(directory.getPath());
					Console.log("New input directory: " + directory.getPath());
				} else {
					Console.log("No new directory chosen");
				}
			}
		});

		in.setCenter(idir);
		in.setRight(infile);

		//Output entry.
		BorderPane out = new BorderPane();

		final TextField ofile = new TextField();
		ofile.setPrefWidth(300);
		ofile.setText(output.getPath());
		ofile.setEditable(false);
		Button outfile = new Button("Output file");
		outfile.setPrefWidth(100);
		outfile.setOnAction(new EventHandler<ActionEvent>(){

			public void handle(ActionEvent arg0) {
				FileChooser out = new FileChooser();
				out.setInitialDirectory(directory);
				out.setInitialFileName("results.csv");
				out.setTitle("Output file");
				File tmp = out.showSaveDialog(window);
				if (tmp != null){
					output = tmp;
					ofile.setText(output.getPath());
					Console.log("New output file: " + output.getName());
				} else {
					Console.log("No new output chosen");
				}
			}

		});

		out.setCenter(ofile);
		out.setRight(outfile);

		HBox bottom = new HBox();


		run.setDisable(true);
		run.setOnAction(new EventHandler<ActionEvent>(){

			public void handle(ActionEvent arg0) {

				if (directory.isDirectory() || directory.isFile()){

					FileParser fr;
					try {
						Pattern p = Pattern.compile(regexFileMatcher.getText());
						String d = delimiter.getValue().replace("<tab>", "\t").replace("<space>", " ");
						fr = new FileParser(directory, output, d, p);
					} catch (IOException e1) {
						Console.warn("An error occured when creating the parser. Check for errors above.");
						return;
					}
					prog.progressProperty().bind(fr.progressProperty());
					status.textProperty().bind(fr.messageProperty());

					fr.setOnSucceeded(new EventHandler<WorkerStateEvent>(){

						@Override
						public void handle(WorkerStateEvent arg0) {
							status.textProperty().unbind();
							status.setText("Finished");
							prog.progressProperty().unbind();
							prog.setProgress(1);
						}

					});

					fr.setOnFailed(new EventHandler<WorkerStateEvent>(){

						@Override
						public void handle(WorkerStateEvent e) {
							status.textProperty().unbind();
							status.setText("Failed. See console");
							Console.error("Failed to parse:", e.getSource().getException().getMessage());
							prog.progressProperty().unbind();
							prog.setProgress(0);
						}

					});

					new Thread(fr).start();

				} else {
					Console.warn("Invalid input/output settings. Please reselect your input and output.");
				}
			}

		});

		bottom.getChildren().addAll(run, prog, status);

		//Add elements.
		list.getChildren().addAll(vars, in, out, bottom);

		return list;

	}

	public static void main(String[] args){

		Console.log("Preparing interfaces");
		Console.log("Preparing GUI");

		launch(Main.class, args);

	}

	public static void printToConsole(final String out){
		if (Platform.isFxApplicationThread()){
			console.appendText(out + "\r\n");
		} else {
			Platform.runLater(new Runnable(){

				@Override
				public void run() {
					console.appendText(out + "\r\n");
				}

			});
		}
	}

	public static void openConsole(){

		console.setPrefHeight(300);
		console.setPrefWidth(500);
		console.setEditable(false);

		Console.register(new ConsoleOutput(){

			public void print(String out) {
				printToConsole(out);
			}

		});

		final Scene s = new Scene(console);
		consoleWindow.setScene(s);

		consoleWindow.setTitle("Console");

		Console.log("Loaded ConsoleGUI");

	}

}
